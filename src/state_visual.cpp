#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>

#include <ros/console.h>
#include <rviz/ogre_helpers/arrow.h>
#include <rviz/default_plugin/covariance_visual.h>
#include <geometry_msgs/Vector3.h>

#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include "state_visual.h"

namespace rviz_multiception
{

void computeStateObjects(const multiception::State& state, rviz::Arrow& arrowVis, rviz::CovarianceVisual& covVis, double yawMax)
{
	// Process the pose arrow, scaled by velocity and oriented by heading
	Ogre::Vector3 pose(state.x, state.y, 1.5);
	arrowVis.setPosition(pose);

	// Special case, if yaw is 0, we consider the state to be static
	// without orientation (e.g: pole) and displaying an orientation makes no sense
	float length = std::min(4.0, std::max(1.0, state.v/5.0));
	if(state.v<=0) arrowVis.setScale(Ogre::Vector3(0.1, 0.1, 0.1));
	else arrowVis.setScale(Ogre::Vector3(length, 1, 1));

	Ogre::Vector3 direction(std::cos(state.O), std::sin(state.O), 0);
	arrowVis.setDirection(direction);

	// Process the covariance ellipsis
	std::array<double, 25> covTot;
	for(size_t i=0;i<covTot.size(); i++) covTot[i] = state.covd[i] + state.covi[i];

	geometry_msgs::PoseWithCovariance cov;
	cov.pose.position.x = state.x; cov.pose.position.y = state.y;
	tf2::Quaternion q; q.setRPY(0.0, 0.0, state.O);
	cov.pose.orientation = tf2::toMsg(q);
	cov.covariance[0*6+0] = covTot[0*5+0]; // xx
	cov.covariance[0*6+1] = covTot[0*5+1]; // xy
	cov.covariance[1*6+0] = covTot[1*5+0]; // yx
	cov.covariance[1*6+1] = covTot[1*5+1]; // yy

	if(state.v>0)
	{
		cov.covariance[0*6+5] = covTot[0*5+2]; // xyaw
		cov.covariance[1*6+5] = covTot[1*5+2]; // yyaw
		cov.covariance[5*6+0] = covTot[2*5+0]; // yawx
		cov.covariance[5*6+1] = covTot[2*5+1]; // yawy
		cov.covariance[5*6+5] = std::max(0., std::min(yawMax*yawMax, covTot[2*5+2])); // yawyaw
	}

	Ogre::Quaternion covq(Ogre::Radian(state.O), Ogre::Vector3::UNIT_Z);
	covVis.setCovariance(cov);
	covVis.setPosition(pose);
	covVis.setOrientation(covq);
}

} // namespace rviz_multiception

