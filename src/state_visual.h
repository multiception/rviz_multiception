#ifndef STATE_VISUAL_H
#define STATE_VISUAL_H

#include <multiception/Perception.h>

namespace rviz
{
class Arrow;
class CovarianceVisual;
}

namespace rviz_multiception
{

void computeStateObjects(const multiception::State& state, rviz::Arrow& arrowVis, rviz::CovarianceVisual& covVis, double yawMax);

} // namespace rviz_multiception

#endif // STATE_VISUAL_H
