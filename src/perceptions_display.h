#ifndef PERCEPTIONS_DISPLAY_H
#define PERCEPTIONS_DISPLAY_H

#ifndef Q_MOC_RUN
#include <vector>

#include <rviz/message_filter_display.h>
#include <OGRE/OgreMaterialManager.h>
#include <multiception/Perceptions.h>
#endif

namespace Ogre
{
class SceneNode;
class ManualObject;
}

namespace rviz
{
class ColorProperty;
class FloatProperty;
class IntProperty;
class Shape;
class BillboardLine;
class MovableText;
class Arrow;
class CovarianceVisual;
class CovarianceProperty;
}

namespace grid_map_rviz_plugin
{
class GridMapVisual;
}

namespace rviz_multiception
{

class PerceptionsDisplay: public rviz::MessageFilterDisplay<multiception::Perceptions>
{
Q_OBJECT
public:
	// Constructor. pluginlib::ClassLoader creates instances by calling
	// the default constructor, so make sure you have one.
	PerceptionsDisplay();
	virtual ~PerceptionsDisplay();

protected:
	virtual void onInitialize();

	// A helper to clear this display back to the initial state.
	virtual void reset();
	void clear();
	void reserveVectors(size_t N=0);

	// Qt slots connected to signals indicating changes in the user-editable properties.
private Q_SLOTS:
	void updateColorAndAlpha();

private:
	// Function to handle an incoming ROS message.
	void processMessage(const multiception::Perceptions::ConstPtr& msg);

	// Storage for the list of visuals
	std::vector<float> percept_existences_;
	std::vector<boost::shared_ptr<rviz::Arrow>> pose_visuals_;
	std::vector<boost::shared_ptr<rviz::Shape>> shape_visuals_;
	std::vector<boost::shared_ptr<rviz::CovarianceVisual>> cov_visuals_;
	std::vector<boost::shared_ptr<rviz::MovableText>> text_visuals_;
	std::vector<boost::shared_ptr<rviz::BillboardLine>> trace_visuals_;
	std::vector<Ogre::SceneNode*> text_nodes_;
	std::vector<boost::shared_ptr<Ogre::ManualObject>> fov_visuals_, fs_visuals_;
	Ogre::MaterialPtr fov_material_, fs_material_;
	std::vector<boost::shared_ptr<grid_map_rviz_plugin::GridMapVisual>> grid_visuals_;

	float trace_duration_;
	Ogre::SceneNode* frame_node_;

	// User-editable property variables.
	rviz::ColorProperty* color_property_;
	rviz::FloatProperty* alpha_property_;
	rviz::BoolProperty* state_property_;
	rviz::BoolProperty* shape_property_;
	rviz::FloatProperty* shape_alpha_property_;
	rviz::BoolProperty* covariance_property_;
	rviz::FloatProperty* covariance_alpha_property_;
	rviz::FloatProperty* covariance_scale_property_;
	rviz::BoolProperty* text_property_;
	rviz::ColorProperty* text_color_property_;
	rviz::FloatProperty* text_alpha_property_;
	rviz::BoolProperty* trace_property_;

	rviz::BoolProperty* fov_property_;
	rviz::FloatProperty* fov_alpha_property_;
	rviz::ColorProperty* fov_color_property_;

	rviz::BoolProperty* fs_property_;
	rviz::FloatProperty* fs_alpha_property_;
	rviz::ColorProperty* fs_color_property_;

	rviz::BoolProperty* grid_property_;
	rviz::FloatProperty* grid_alpha_property_;
};

} // namespace rviz_multiception

#endif // PERCEPTIONS_DISPLAY_H
