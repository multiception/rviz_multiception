#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <tf/transform_listener.h>

#include <rviz/visualization_manager.h>
#include <rviz/properties/color_property.h>
#include <rviz/properties/float_property.h>
#include <rviz/default_plugin/covariance_visual.h>
#include <rviz/properties/int_property.h>
#include <rviz/ogre_helpers/shape.h>
#include <rviz/ogre_helpers/arrow.h>
#include <rviz/ogre_helpers/movable_text.h>
#include <rviz/ogre_helpers/billboard_line.h>
#include <rviz/frame_manager.h>
#include <OGRE/OgreManualObject.h>
#include <rviz_map/paint_helper.h>
#include <perception_utils/polygons.h>
#include <grid_map_rviz_plugin/GridMapVisual.hpp>

#include "state_visual.h"
#include "perceptions_display.h"

namespace rviz_multiception
{

PerceptionsDisplay::PerceptionsDisplay()
{
	color_property_ = new rviz::ColorProperty("Color", QColor(10,10,10), "Color applied to all perceptions.", this, SLOT( updateColorAndAlpha() ));
	alpha_property_ = new rviz::FloatProperty("Alpha", 1.0, "Transparency applied to all perceptions.", this, SLOT( updateColorAndAlpha() ));
	alpha_property_->setMin(0); alpha_property_->setMax(1);

	state_property_ = new rviz::BoolProperty("State", true, "Wether to display object states.", this);
	shape_property_ = new rviz::BoolProperty("Shape", true, "Wether to display object shapes.", this);
	shape_alpha_property_ = new rviz::FloatProperty("Alpha", 0.4, "Shape transparency.", shape_property_);
	shape_property_->setDisableChildrenIfFalse(true);
	shape_alpha_property_->setMin(0); shape_alpha_property_->setMax(1);

	covariance_property_ = new rviz::BoolProperty("Covariance", true, "Wether to display covariance ellipsis.", this);
	covariance_alpha_property_ = new rviz::FloatProperty("Alpha", 0.4, "Covariance ellipsis transparency.", covariance_property_);
	covariance_scale_property_ = new rviz::FloatProperty("Scale", 2, "Covariance ellipsis scale (number of standard deviations).", covariance_property_);
	covariance_property_->setDisableChildrenIfFalse(true);
	covariance_alpha_property_->setMin(0); covariance_alpha_property_->setMax(1);
	covariance_scale_property_->setMin(0);

	text_property_ = new rviz::BoolProperty("Debug Text", false, "Wether to display debug text.", this);
	text_alpha_property_ = new rviz::FloatProperty("Alpha", 1, "Text transparency.", text_property_);
	text_color_property_ = new rviz::ColorProperty("Color", QColor(10,10,10), "Text Color.", text_property_);
	text_property_->setDisableChildrenIfFalse(true);
	text_alpha_property_->setMin(0); text_alpha_property_->setMax(1);

	trace_property_ = new rviz::BoolProperty("Trace", true, "Wether to display object traces.", this);

	fov_property_ = new rviz::BoolProperty("FOV", false, "Wether to display Field of View.", this);
	fov_alpha_property_ = new rviz::FloatProperty("Alpha", 1, "Polygon transparency.", fov_property_);
	fov_color_property_ = new rviz::ColorProperty("Color Min", QColor(0, 255, 0), "Polygon color.", fov_property_);

	fs_property_ = new rviz::BoolProperty("FS", false, "Wether to display Free Space.", this);
	fs_alpha_property_ = new rviz::FloatProperty("Alpha", 1, "Polygon transparency.", fs_property_);
	fs_color_property_ = new rviz::ColorProperty("Color", QColor(255, 0, 0), "Polygon color.", fs_property_);

	grid_property_ = new rviz::BoolProperty("Observability Grid", false, "Wether to display observability grids.", this);
	grid_alpha_property_ = new rviz::FloatProperty("Alpha", 0.5, "Grid transparency.", grid_property_);
}

void PerceptionsDisplay::onInitialize()
{
	MFDClass::onInitialize();
	frame_node_ = scene_node_->createChildSceneNode();

	auto initMaterial = [](const std::string& name)
	{
		Ogre::MaterialPtr m = Ogre::MaterialManager::getSingleton().create(name+std::to_string(rand()), "General");
		m->setReceiveShadows(false);
		m->getTechnique(0)->setLightingEnabled(true);
		return m;
	};

	fov_material_ = initMaterial("fov");
	fs_material_ = initMaterial("fs");
	updateColorAndAlpha();
}

PerceptionsDisplay::~PerceptionsDisplay()
{
	reset();
	scene_manager_->destroySceneNode(frame_node_);
}

// Clear the visuals by deleting their objects.
void PerceptionsDisplay::reset()
{
	MFDClass::reset();
	clear();
}

void PerceptionsDisplay::clear()
{
	percept_existences_.clear();
	pose_visuals_.clear();
	shape_visuals_.clear();
	cov_visuals_.clear();
	text_visuals_.clear();
	trace_visuals_.clear();
	for(Ogre::SceneNode* textNode: text_nodes_)
		scene_manager_->destroySceneNode(textNode);
	text_nodes_.clear();

	for(const boost::shared_ptr<Ogre::ManualObject>& o : fov_visuals_) frame_node_->detachObject(o.get());
	fov_visuals_.clear();
	for(const boost::shared_ptr<Ogre::ManualObject>& o : fs_visuals_) frame_node_->detachObject(o.get());
	fs_visuals_.clear();

	grid_visuals_.clear();
}

void PerceptionsDisplay::reserveVectors(size_t N)
{
	percept_existences_.reserve(N);
	pose_visuals_.reserve(N);
	shape_visuals_.reserve(N);
	cov_visuals_.reserve(N);
	text_visuals_.reserve(N);
	text_visuals_.reserve(N);
	text_nodes_.reserve(N);
}

// Set the current color and alpha values for each visual.
void PerceptionsDisplay::updateColorAndAlpha()
{
	const float alpha = alpha_property_->getFloat();
	const float shape_alpha = shape_alpha_property_->getFloat();
	const float covariance_alpha = covariance_alpha_property_->getFloat();
	const float covariance_scale = covariance_scale_property_->getFloat();
	const float fov_alpha = fov_alpha_property_->getFloat();
	const float fs_alpha = fs_alpha_property_->getFloat();

	const Ogre::ColourValue color = color_property_->getOgreColor();
	Ogre::ColourValue textColor = text_color_property_->getOgreColor();
	textColor.a = text_alpha_property_->getFloat();

	Ogre::ColourValue fovColor = fov_color_property_->getOgreColor();
	fovColor.a = fov_alpha;

	Ogre::ColourValue fsColor = fs_color_property_->getOgreColor();
	fsColor.a = fs_alpha;

	for(size_t i=0; i<pose_visuals_.size(); i++)
	{
		pose_visuals_[i]->setColor(color.r, color.g, color.b, alpha*percept_existences_[i]);
	}

	for(size_t i=0; i<shape_visuals_.size(); i++)
	{
		shape_visuals_[i]->setColor(color.r, color.g, color.b, alpha*shape_alpha*percept_existences_[i]);
	}

	for(size_t i=0; i<cov_visuals_.size(); i++)
	{
		cov_visuals_[i]->setPositionColor(color.r, color.g, color.b, alpha*covariance_alpha*percept_existences_[i]);
		cov_visuals_[i]->setOrientationColor(color.r, color.g, color.b, alpha*covariance_alpha*percept_existences_[i]);
		cov_visuals_[i]->setScales(covariance_scale, covariance_scale);
		cov_visuals_[i]->setOrientationOffset(1);
	}

	for(size_t i=0; i<trace_visuals_.size(); i++)
	{
		trace_visuals_[i]->setColor(color.r, color.g, color.b, alpha*percept_existences_[i]);
	}

	for(size_t i=0; i<text_visuals_.size(); i++)
	{
		text_visuals_[i]->setColor(textColor);
	}

	auto setFovColor = [](Ogre::MaterialPtr& m, const Ogre::ColourValue& color, float alpha)
	{
		m->getTechnique(0)->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
		m->getTechnique(0)->setDepthWriteEnabled(false);
		m->getTechnique(0)->setDiffuse(Ogre::ColourValue(0,0,0, alpha));
		m->getTechnique(0)->setAmbient(Ogre::ColourValue(0,0,0, alpha));
		m->getTechnique(0)->setSelfIllumination(color);
	};

	setFovColor(fov_material_, fovColor, fov_alpha);
	setFovColor(fs_material_, fsColor, fs_alpha);
}

void PerceptionsDisplay::processMessage(const multiception::Perceptions::ConstPtr& msg)
{
	clear();

	Ogre::Vector3 position;
	Ogre::Quaternion orientation;
	if(context_->getFrameManager()->getTransform(msg->header.frame_id, msg->header.stamp, position, orientation))
	{
		frame_node_->setPosition(position);
		frame_node_->setOrientation(orientation);
	}
	else
	{
		ROS_DEBUG("Error transforming from frame '%s' to frame '%s'", msg->header.frame_id.c_str(), qPrintable(fixed_frame_));
		return;
	}

	// Process the sources FOVs
	if(fov_property_->getBool())
	for(const auto& s : msg->sources)
	{
		perception_utils::Polygon3 fov;
		perception_utils::convert::jskToEigen(s.fovmin, fov);
		boost::shared_ptr<Ogre::ManualObject> fovVisual { scene_manager_->createManualObject() };
		paintPolygon(fov + Eigen::Vector3f(0, 0, 0.3).replicate(1, fov.cols()), fovVisual.get(), fov_material_->getName());
		frame_node_->attachObject(fovVisual.get());
		fov_visuals_.push_back(fovVisual);
	}

	if(fs_property_->getBool())
	for(const auto& s : msg->sources)
	{
		perception_utils::Polygon3 fs;
		perception_utils::convert::jskToEigen(s.freesp, fs);
		boost::shared_ptr<Ogre::ManualObject> fsVisual { scene_manager_->createManualObject() };
		paintPolygon(fs + Eigen::Vector3f(0, 0, 0.2).replicate(1, fs.cols()), fsVisual.get(), fs_material_->getName());
		frame_node_->attachObject(fsVisual.get());
		fs_visuals_.push_back(fsVisual);
	}

	if(grid_property_->getBool())
	for(const auto& s : msg->sources)
	{
		if(s.observability.layers.size()!=4 || s.observability.data.size()!=4)
		{
			ROS_WARN("Observability grid is of wrong dimension");
			continue;
		}

		boost::shared_ptr<grid_map_rviz_plugin::GridMapVisual> gridVisual { new grid_map_rviz_plugin::GridMapVisual(scene_manager_, frame_node_) };
		boost::shared_ptr<grid_map_msgs::GridMap> gridmapmsg { new grid_map_msgs::GridMap(s.observability) };
		// As the conflict is not expected to be useful (for display purposes at least)
		// it is used for display purposes, showing the information between -1 (O) and 1 (o)
		// by doing "" = "o"-"O"
		for(size_t i=0; i<gridmapmsg->data[0].data.size(); i++)
		{
			gridmapmsg->data[0].data[i] = 1-(gridmapmsg->data[1].data[i] - gridmapmsg->data[2].data[i]);
			gridmapmsg->data[3].data[i] = (gridmapmsg->data[3].data[i]<0.999) ? gridmapmsg->data[3].data[i] : NAN;
		}

		gridVisual->setMessage(gridmapmsg);
		const float gridAlpha = alpha_property_->getFloat() * grid_alpha_property_->getFloat();
		gridVisual->computeVisualization(gridAlpha, false, true, "o,O", false, false, Ogre::ColourValue(0.8, 0.8, 0.8), false, "", "default", true, false, Ogre::ColourValue(0, 0, 0), Ogre::ColourValue(), false, -1, 1, 0.1, 1);
		grid_visuals_.push_back(gridVisual);
	}

	if(msg->perceptions.size()>0)
	{
		reserveVectors(msg->perceptions.size());
	}
	else
	{
		return;
	}

	// Process the perceptions in the message
	for(const multiception::Perception& perception : msg->perceptions)
	{
		const float z = (perception.height>0.1) ? perception.height/2+.1 : 1;

		// Process the state
		if(state_property_->getBool() || covariance_property_->getBool())
		{
			boost::shared_ptr<rviz::Arrow> arrowVis { new rviz::Arrow(scene_manager_, frame_node_) };
			boost::shared_ptr<rviz::CovarianceVisual> covVis { new rviz::CovarianceVisual(scene_manager_, frame_node_ ,true) };
			computeStateObjects(perception.state, *arrowVis, *covVis, M_PI_4/covariance_scale_property_->getFloat());
			if(state_property_->getBool()) pose_visuals_.push_back(arrowVis);
			if(covariance_property_->getBool()) cov_visuals_.push_back(covVis);
		}

		// Process the shape
		if(shape_property_->getBool())
		{
			boost::shared_ptr<rviz::Shape> shapeVis { new rviz::Shape(rviz::Shape::Cube, scene_manager_, frame_node_) };
			shapeVis->setScale( Ogre::Vector3(perception.length, perception.width, std::max(0.1f, perception.height)) );
			shapeVis->setPosition( Ogre::Vector3(perception.state.x, perception.state.y, z) );
			shapeVis->setOrientation( Ogre::Quaternion(Ogre::Radian(perception.state.O), Ogre::Vector3::UNIT_Z) );
			shape_visuals_.push_back(shapeVis);
			const auto existenceIter = std::find(perception.existence.frame.cbegin(), perception.existence.frame.cend(), "e");
			if(existenceIter!=perception.existence.frame.cend()) {
				const size_t existenceIdx = existenceIter - perception.existence.frame.cbegin();
				percept_existences_.push_back(perception.existence.values[std::pow(2, existenceIdx)]);
			}
		}

		// Process the debug text
		if(!perception.debug.empty() && text_property_->getBool())
		{
			Ogre::SceneNode* childNode = frame_node_->createChildSceneNode();
			boost::shared_ptr<rviz::MovableText> debugText { new rviz::MovableText(perception.debug, "Liberation Sans", .7) };
			debugText->setTextAlignment(rviz::MovableText::H_CENTER, rviz::MovableText::V_CENTER);
			childNode->setPosition(Ogre::Vector3(perception.state.x, perception.state.y, z+perception.height/2+.1+1) );
			childNode->attachObject(debugText.get());
			text_visuals_.push_back(debugText);
			text_nodes_.push_back(childNode);
		}

		// Process the buffered trace
		if(perception.trace.size()>0 && trace_property_->getBool())
		{
			boost::shared_ptr<rviz::BillboardLine> traceVis { new rviz::BillboardLine(scene_manager_, frame_node_) };
			for(size_t i=0; i<perception.trace.size(); i+=2)
			{
				traceVis->addPoint(Ogre::Vector3(perception.trace[i], perception.trace[i+1], 0));
			}
			trace_visuals_.push_back(traceVis);
		}
	}

	updateColorAndAlpha();
	context_->queueRender();
}

} // end namespace rviz_multiception

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(rviz_multiception::PerceptionsDisplay,rviz::Display)
