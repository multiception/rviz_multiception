#ifndef STATE_DISPLAY_H
#define STATE_DISPLAY_H

#ifndef Q_MOC_RUN
#include <vector>

#include <rviz/message_filter_display.h>
#include <multiception/StateStamped.h>
#endif

namespace Ogre
{
class SceneNode;
}

namespace rviz
{
class Arrow;
class CovarianceVisual;
class ColorProperty;
class FloatProperty;
class IntProperty;
class CovarianceProperty;
}

namespace rviz_multiception
{

class StateDisplay: public rviz::MessageFilterDisplay<multiception::StateStamped>
{
Q_OBJECT
public:
	// Constructor. pluginlib::ClassLoader creates instances by calling
	// the default constructor, so make sure you have one.
	StateDisplay();
	virtual ~StateDisplay();

	// Overrides of protected virtual functions from Display. As much
	// as possible, when Displays are not enabled, they should not be
	// subscribed to incoming data and should not show anything in the
	// 3D view. These functions are where these connections are made
	// and broken.
protected:
	virtual void onInitialize();

	// A helper to clear this display back to the initial state.
	virtual void reset();
	void clear();

	// Qt slots connected to signals indicating changes in the user-editable properties.
private Q_SLOTS:
	void updateColorAndAlpha();

private:
	// Function to handle an incoming ROS message.
	void processMessage(const multiception::StateStamped::ConstPtr& msg);

	// Storage of the visual
	boost::shared_ptr<rviz::Arrow> pose_visual_;
	boost::shared_ptr<rviz::CovarianceVisual> cov_visual_;

	Ogre::SceneNode* frame_node_;

	// User-editable property variables.
	rviz::ColorProperty* color_property_;
	rviz::FloatProperty* alpha_property_;
	rviz::BoolProperty* covariance_property_;
	rviz::FloatProperty* covariance_alpha_property_;
	rviz::FloatProperty* covariance_scale_property_;
};

} // namespace rviz_multiception

#endif // STATE_DISPLAY_H
