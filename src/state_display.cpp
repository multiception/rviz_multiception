#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <tf/transform_listener.h>

#include <rviz/visualization_manager.h>
#include <rviz/ogre_helpers/arrow.h>
#include <rviz/default_plugin/covariance_visual.h>
#include <rviz/properties/color_property.h>
#include <rviz/properties/float_property.h>
#include <rviz/default_plugin/covariance_property.h>
#include <rviz/properties/int_property.h>
#include <rviz/frame_manager.h>

#include "state_visual.h"
#include "state_display.h"

namespace rviz_multiception
{

StateDisplay::StateDisplay()
{
	color_property_ = new rviz::ColorProperty("Color", QColor(10,10,10), "Color applied to all perceptions.", this, SLOT( updateColorAndAlpha() ));
	alpha_property_ = new rviz::FloatProperty("Alpha", 1.0, "Transparency applied to all perceptions.", this, SLOT( updateColorAndAlpha() ));
	alpha_property_->setMin(0); alpha_property_->setMax(1);

	covariance_property_ = new rviz::BoolProperty("Covariance", true, "Wether to display covariance ellipsis.", this);
	covariance_alpha_property_ = new rviz::FloatProperty("Alpha", 0.4, "Covariance ellipsis transparency.", covariance_property_);
	covariance_scale_property_ = new rviz::FloatProperty("Scale", 2, "Covariance ellipsis scale (number of standard deviations).", covariance_property_);
	covariance_property_->setDisableChildrenIfFalse(true);
	covariance_alpha_property_->setMin(0); covariance_alpha_property_->setMax(1);
	covariance_scale_property_->setMin(0);
}

void StateDisplay::onInitialize()
{
	MFDClass::onInitialize();
	frame_node_ = scene_node_->createChildSceneNode();
}

StateDisplay::~StateDisplay()
{
	scene_manager_->destroySceneNode(frame_node_);
}

void StateDisplay::reset()
{
	MFDClass::reset();
	clear();
}

void StateDisplay::clear()
{
	pose_visual_.reset();
	cov_visual_.reset();
}

void StateDisplay::updateColorAndAlpha()
{
	if(pose_visual_)
	{
		const float alpha = alpha_property_->getFloat();
		const float covariance_alpha = covariance_alpha_property_->getFloat();
		const float covariance_scale = covariance_scale_property_->getFloat();
		const Ogre::ColourValue color = color_property_->getOgreColor();

		pose_visual_->setColor(color.r, color.g, color.b, alpha);
		cov_visual_->setPositionColor(color.r, color.g, color.b, alpha*covariance_alpha);
		cov_visual_->setOrientationColor(color.r, color.g, color.b, alpha*covariance_alpha);
		cov_visual_->setScales(covariance_scale, covariance_scale);
		cov_visual_->setOrientationOffset(1);
	}
}

void StateDisplay::processMessage(const multiception::StateStamped::ConstPtr& msg)
{
	clear();
	Ogre::Vector3 position;
	Ogre::Quaternion orientation;
	if(context_->getFrameManager()->getTransform(msg->header.frame_id, msg->header.stamp, position, orientation))
	{
		frame_node_->setPosition(position);
		frame_node_->setOrientation(orientation);
	}
	else
	{
		ROS_DEBUG("Error transforming from frame '%s' to frame '%s'", msg->header.frame_id.c_str(), qPrintable(fixed_frame_));
		return;
	}

	pose_visual_.reset( new rviz::Arrow(scene_manager_, frame_node_) );
	cov_visual_.reset( new rviz::CovarianceVisual(scene_manager_, frame_node_, true) );

	computeStateObjects(msg->state, *pose_visual_, *cov_visual_, M_PI_2/covariance_scale_property_->getFloat());
	updateColorAndAlpha();
	context_->queueRender();
}

} // end namespace rviz_multiception

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(rviz_multiception::StateDisplay,rviz::Display)
